import { NestFactory } from '@nestjs/core';
import {
  FastifyAdapter,
  NestFastifyApplication,
} from '@nestjs/platform-fastify';
import { AppModule } from './app.module';
// import fastifyMultipart from 'fastify-multipart';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { APIPrefix } from './common/common';
import { ConfigService } from './config/config.service';
import { ExceptionInterceptor } from './core/interceptors/exception.interceptor';
import { FilterQueryPipe } from './core/pipe/filter-query.pipe';
import { SortQueryPipe } from './core/pipe/sort-query.pipe';

// eslint-disable-next-line @typescript-eslint/no-var-requires
const fastifyMultipart = require('@fastify/multipart');

async function bootstrap() {
  const fastifyAdapter = new FastifyAdapter();
  fastifyAdapter.register(fastifyMultipart, {
    attachFieldsToBody: true,
    addToBody: true,
  });

  const app = await NestFactory.create<NestFastifyApplication>(
    AppModule,
    fastifyAdapter,
  );

  const options = new DocumentBuilder()
    .setTitle('API docs')
    .addTag('users')
    .addTag('tasks')
    .addBearerAuth()
    .setVersion('1.0')
    .build();
  app.setGlobalPrefix(APIPrefix.Version);
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api', app, document);

  const configService = new ConfigService();

  const corsOptions: any = configService.get('corsOrigin')
    ? { origin: configService.get('corsOrigin') }
    : {};
  // eslint-disable-next-line @typescript-eslint/no-var-requires
  app.register(require('@fastify/cors'), corsOptions);

  app.useGlobalPipes(new FilterQueryPipe());
  app.useGlobalPipes(new SortQueryPipe());
  app.useGlobalInterceptors(new ExceptionInterceptor());
  await app.listen(configService.get('port'), '0.0.0.0');
}
bootstrap();
