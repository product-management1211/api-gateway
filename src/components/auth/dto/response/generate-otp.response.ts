import { Expose } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';
import { SuccessResponse } from 'src/utils/success.response.dto';

export class MetaData {
  @Expose()
  email: string;
}

export class GenerateOtpResponseDto extends SuccessResponse {
  @ApiProperty({ example: { email: 'example@gmail.com' } })
  @Expose()
  data: MetaData;
}
