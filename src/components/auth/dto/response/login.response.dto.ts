import { ApiProperty } from '@nestjs/swagger';
import { Expose, Type } from 'class-transformer';
import { UserResponseDto } from 'src/components/user/dto/user/response/user.response.dto';

class Token {
  @ApiProperty({})
  @Expose()
  token: string;

  @ApiProperty({})
  @Expose()
  expiresIn: number;
}

export class LoginResponseDto {
  @ApiProperty({
    type: UserResponseDto,
  })
  @Expose()
  @Type(() => UserResponseDto)
  userInfo: UserResponseDto;

  @ApiProperty({
    type: Token,
  })
  @Expose()
  @Type(() => Token)
  accessToken: Token;

  @ApiProperty({
    type: Token,
  })
  @Expose()
  @Type(() => Token)
  refreshToken: Token;

  @ApiProperty()
  @Expose()
  rememberPassword: number;
}
