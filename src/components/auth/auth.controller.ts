import {
  Body,
  Controller,
  Get,
  Inject,
  Post,
  Query,
  Req,
} from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Public } from 'src/core/decorators/set-public.decorator';
import { SuccessResponse } from 'src/utils/success.response.dto';
import { CheckOtpRequestDto } from './dto/request/check-otp.request.dto';
import { GenerateOtpRequestDto } from './dto/request/generate-otp.request.dto';
import { LoginRequestDto } from './dto/request/login.request.dto';
import { ResetPasswordRequestDto } from './dto/request/reset-password.request.dto';
import { GenerateOtpResponseDto } from './dto/response/generate-otp.response';
import { LoginResponseDto } from './dto/response/login.response.dto';

@Controller('auth')
@ApiTags('Auth')
export class AuthController {
  constructor(
    @Inject('USER_SERVICE')
    private readonly userServiceClient: ClientProxy,
  ) {}

  @Get('ping')
  public async get(): Promise<any> {
    return await this.userServiceClient.send('ping', {});
  }

  @Public()
  @Post('/login')
  @ApiOperation({
    tags: ['Auth', 'Login'],
    summary: 'Login',
    description: 'Đăng nhập',
  })
  @ApiResponse({
    status: 200,
    description: 'Login successfully',
    type: LoginResponseDto,
  })
  public async login(@Body() request: LoginRequestDto): Promise<any> {
    return await this.userServiceClient.send('login', request);
  }

  @Get('/token/refresh')
  @ApiOperation({
    tags: ['Auth', 'Refresh Token'],
    summary: 'Login',
    description: 'Tạo mới access token',
  })
  @ApiResponse({
    status: 200,
    description: 'Create successfully',
    type: LoginResponseDto,
  })
  public async refreshToken(
    @Req() request: any,
    @Query('rememberPassword') rememberPassword: number,
  ): Promise<any> {
    const payload = {
      ...request.user,
      rememberPassword,
    };

    return await this.userServiceClient.send('refresh_token', payload);
  }

  //forgot password
  @Public()
  @Post('/forgot-password/generate')
  @ApiOperation({
    tags: ['User'],
    summary: 'Generate opt code',
    description: 'tạo opt code',
  })
  @ApiResponse({
    status: 200,
    description: 'Generate opt code successfully',
    type: GenerateOtpResponseDto,
  })
  public async generateOtp(
    @Body() request: GenerateOtpRequestDto,
  ): Promise<any> {
    return await this.userServiceClient.send('generate_otp', request);
  }

  @Public()
  @Post('/forgot-password/otp')
  @ApiOperation({
    tags: ['User'],
    summary: 'Check opt code',
    description: 'kiểm tra opt code',
  })
  @ApiResponse({
    status: 200,
    description: 'Check opt code successfully',
    type: GenerateOtpResponseDto,
  })
  public async checkOtp(@Body() request: CheckOtpRequestDto): Promise<any> {
    return await this.userServiceClient.send('check_opt', request);
  }

  @Public()
  @Post('/forgot-password/password')
  @ApiOperation({
    tags: ['User'],
    summary: 'Reset password',
    description: 'Thay đổi password',
  })
  @ApiResponse({
    status: 200,
    description: 'Reset password successfully',
    type: SuccessResponse,
  })
  public async forgotPasswordResetPassword(
    @Body() request: ResetPasswordRequestDto,
  ): Promise<any> {
    return await this.userServiceClient.send('reset_password', request);
  }
}
