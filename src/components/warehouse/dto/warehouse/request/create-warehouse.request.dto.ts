import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import {
  ArrayUnique,
  IsArray,
  IsInt,
  IsNotEmpty,
  IsOptional,
  IsString,
  MaxLength,
} from 'class-validator';

export class WarehouseTypeSetting {
  @ApiProperty({ example: 1, description: 'warehouse type id' })
  @IsInt()
  @IsNotEmpty()
  id: number;
}

export class CreateWarehouseRequestDto {
  @ApiProperty({ example: 'Warehouse 1', description: 'warehouse name' })
  @IsString()
  @IsNotEmpty()
  @MaxLength(255)
  name: string;

  @ApiProperty({ example: 1, description: 'warehouse factoryId' })
  @IsInt()
  @IsNotEmpty()
  factoryId: number;

  @ApiProperty({ example: 1, description: 'warehouse companyId' })
  @IsInt()
  @IsNotEmpty()
  companyId: number;

  @ApiProperty({ example: 'ABCD', description: 'warehouse code' })
  @IsString()
  @MaxLength(4)
  @IsNotEmpty()
  code: string;

  @ApiPropertyOptional({
    example: 'This is warehouse 1',
    description: 'warehouse description',
  })
  @IsString()
  @MaxLength(255)
  @IsOptional()
  description: string;

  @ApiPropertyOptional({
    example: 'Ha Noi City',
    description: 'warehouse location',
  })
  @IsString()
  @IsOptional()
  @MaxLength(255)
  location: string;

  @ApiPropertyOptional({ type: [WarehouseTypeSetting] })
  @IsOptional()
  @IsArray()
  @ArrayUnique<WarehouseTypeSetting>((e: WarehouseTypeSetting) => e.id)
  @Type(() => WarehouseTypeSetting)
  warehouseTypeSettings: WarehouseTypeSetting[];
}
