import { Body, Controller, Get, Inject, Post } from "@nestjs/common";
import { ClientProxy } from "@nestjs/microservices";
import { ApiOperation, ApiResponse, ApiTags } from "@nestjs/swagger";
import { CreateWarehouseRequestDto } from "./dto/warehouse/request/create-warehouse.request.dto";
import { WarehouseResponseDto } from "./dto/warehouse/response/warehouse.response.dto";

@Controller('warehouses')
@ApiTags('Warehouses')
export class WarehouseController {
    constructor(
        @Inject('WAREHOUSE_SERVICE')
        private readonly warehouseServiceClient: ClientProxy
    ) { }

    @Get('ping')
    public async get(): Promise<any> {
        return await this.warehouseServiceClient.send('ping', {});
    }

    @Post('/create')
    @ApiOperation({
        tags: ['Warehouse'],
        summary: 'Create Warehouse',
        description: 'Tạo mới kho',
    })
    @ApiResponse({
        status: 200,
        description: 'Create successfully',
        type: WarehouseResponseDto,
    })
    public async create(
        @Body() request: CreateWarehouseRequestDto,
    ): Promise<any> {
        return await this.warehouseServiceClient.send('create', request);
    }
}