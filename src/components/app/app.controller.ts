import { Controller, Get, Inject } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { ApiTags } from '@nestjs/swagger';

@Controller('app')
@ApiTags('App')
export class AppController {
  constructor(
    @Inject('USER_SERVICE') private readonly userServiceClient: ClientProxy,
  ) {}

  @Get('ping')
  public async get(): Promise<any> {
    console.log('hello');

    return await this.userServiceClient.send('ping', {});
  }
}
