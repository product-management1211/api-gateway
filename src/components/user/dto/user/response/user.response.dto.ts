import { ApiProperty } from '@nestjs/swagger';
import { Expose, Type } from 'class-transformer';

class WarehouseResponse {
  @ApiProperty({ example: 1, description: '' })
  @Expose()
  id: number;
}

class UserRoleSettingResponse {
  @ApiProperty({ example: 1, description: '' })
  @Expose()
  id: number;
}

class FactoryResponse {
  @ApiProperty({ example: 1, description: '' })
  @Expose()
  id: number;
}

class DepartmentSettingResponse {
  @ApiProperty({ example: 1, description: '' })
  @Expose()
  id: number;
}

export class UserResponseDto {
  @ApiProperty({ example: 1, description: '' })
  @Expose()
  id: number;

  @ApiProperty({ example: 'abc@gmail.com', description: '' })
  @Expose()
  email: string;

  @ApiProperty({ example: 'abc', description: '' })
  @Expose()
  username: string;

  @ApiProperty({ example: 'abc', description: '' })
  @Expose()
  fullName: string;

  @ApiProperty({ example: 1, description: '' })
  @Expose()
  companyId: string;

  @ApiProperty({ example: '2021-07-01', description: '' })
  @Expose()
  dateOfBirth: string;

  @ApiProperty({ example: 'abc', description: '' })
  @Expose()
  code: string;

  @ApiProperty({ example: '0987-1254-125', description: '' })
  @Expose()
  phone: string;

  @ApiProperty({ example: 1, description: '' })
  @Expose()
  status: number;

  @Expose()
  createdAt: string;

  @Expose()
  updatedAt: string;

  @ApiProperty({
    type: WarehouseResponse,
    example: [{ id: 1, name: 'warehouse 1' }],
    description: '',
  })
  @Expose()
  @Type(() => WarehouseResponse)
  userWarehouses: WarehouseResponse[];

  @ApiProperty({
    type: UserRoleSettingResponse,
    example: [{ id: 1, name: 'role 1' }],
    description: '',
  })
  @Expose()
  @Type(() => UserRoleSettingResponse)
  userRoleSettings: UserRoleSettingResponse[];

  @ApiProperty({
    type: DepartmentSettingResponse,
    example: [{ id: 1, name: 'department 1' }],
    description: '',
  })
  @Expose()
  @Type(() => DepartmentSettingResponse)
  departmentSettings: DepartmentSettingResponse[];

  @ApiProperty({
    type: FactoryResponse,
    example: [{ id: 1, name: 'factory 1' }],
    description: '',
  })
  @Expose()
  @Type(() => FactoryResponse)
  factories: FactoryResponse[];
}
