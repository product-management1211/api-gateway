import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import {
  IsOptional,
  IsString,
} from 'class-validator';
import { PaginationQuery } from 'src/utils/pagination.query';



export class GetListUserRequestDto extends PaginationQuery {
  @ApiPropertyOptional({ example: 'user', description: '' })
  @IsOptional()
  @IsString()
  keyword?: string;
}
