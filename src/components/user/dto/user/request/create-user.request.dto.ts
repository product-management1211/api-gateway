import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, MaxLength, MinLength } from 'class-validator';
import { AbstractUserRequestDto } from '../request/abstract-user.request.dto';

export class CreateUserRequestDto extends AbstractUserRequestDto {
  @ApiProperty({ example: '121199', description: 'password' })
  @IsNotEmpty()
  @MinLength(6)
  @MaxLength(255)
  password: string;
}
