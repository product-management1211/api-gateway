import { AbstractUserRequestDto } from './abstract-user.request.dto';

export class UpdateUserRequestDto extends AbstractUserRequestDto {}
