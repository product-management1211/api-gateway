import { ApiProperty } from '@nestjs/swagger';
import { Expose } from 'class-transformer';
import { SuccessResponse } from 'src/utils/success.response.dto';

export class FactoryDto {
  @ApiProperty({ example: 1, description: '' })
  @Expose()
  id: number;

  @ApiProperty({ example: 2, description: '' })
  @Expose()
  companyId: number;

  @ApiProperty({ example: 'factory 1', description: '' })
  @Expose()
  name: string;

  @ApiProperty({ example: 'ABCDEF', description: '' })
  @Expose()
  code: string;

  @ApiProperty({ example: 'Ha Noi', description: '' })
  @Expose()
  location: string;

  @ApiProperty({ example: '0955-015-1458', description: '' })
  @Expose()
  phone: string;

  @ApiProperty({ example: 'description', description: '' })
  @Expose()
  description: string;

  @ApiProperty({ example: '2021-07-13 09:13:15.562609+00', description: '' })
  @Expose()
  createdAt: Date;

  @ApiProperty({ example: '2021-07-13 09:13:15.562609+00', description: '' })
  @Expose()
  updatedAt: Date;
}

export class FactoryResponseDto extends SuccessResponse {
  @ApiProperty({
    example: {
      id: 1,
      companyId: 1,
      name: 'factory 1',
      code: 'ABCD',
      description: 'description',
      location: 'location',
      phone: 'phone',
      createdAt: '2021-07-14T02:48:36.864Z',
      updatedAt: '2021-07-14T02:48:36.864Z',
    },
  })
  @Expose()
  data: FactoryResponseDto;
}
