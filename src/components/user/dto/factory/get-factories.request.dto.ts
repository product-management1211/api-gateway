import { PaginationQuery } from 'src/utils/pagination.query';

export class GetFactoriesRequestDto extends PaginationQuery {}
