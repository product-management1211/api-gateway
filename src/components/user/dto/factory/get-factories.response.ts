import { ApiProperty } from '@nestjs/swagger';
import { Expose } from 'class-transformer';
import { SuccessResponse } from 'src/utils/success.response.dto';
import { FactoryResponseDto } from './factory.response.dto';

class Meta {
  @Expose()
  total: number;

  @Expose()
  page: number;
}

class MetaData {
  @Expose()
  data: FactoryResponseDto[];

  @Expose()
  meta: Meta;
}

export class GetFactoriesResponseDto extends SuccessResponse {
  @ApiProperty({
    example: {
      items: [
        {
          id: 1,
          companyId: 1,
          name: 'factory 1',
          code: 'ABCD',
          location: 'location',
          phone: 'phone',
          description: 'description',
          createdAt: '2021-07-14T02:48:36.864Z',
          updatedAt: '2021-07-14T02:48:36.864Z',
        },
        {
          id: 2,
          companyId: 1,
          name: 'factory 2',
          code: 'ABCE',
          location: 'location',
          phone: 'phone',
          description: 'description',
          createdAt: '2021-07-14T02:48:36.864Z',
          updatedAt: '2021-07-14T02:48:36.864Z',
        },
      ],
      meta: {
        total: 2,
        page: 1,
      },
    },
  })
  @Expose()
  data: MetaData;
}
