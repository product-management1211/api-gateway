import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import {
  IsNotEmpty,
  IsString,
  IsOptional,
  MaxLength,
  IsInt,
} from 'class-validator';

export class CreateFactoryRequestDto {
  @ApiProperty({ example: '1', description: '' })
  @IsInt()
  @IsNotEmpty()
  companyId: number;

  @ApiProperty({ example: 'factory 1', description: '' })
  @IsString()
  @IsNotEmpty()
  @MaxLength(255)
  name: string;

  @ApiProperty({ example: 'ABCD', description: '' })
  @IsString()
  @MaxLength(9)
  @IsNotEmpty()
  code: string;

  @ApiPropertyOptional({ example: 'description', description: '' })
  @IsString()
  @IsOptional()
  @MaxLength(255)
  location: string;

  @ApiPropertyOptional({ example: 'phone', description: '' })
  @IsString()
  @IsOptional()
  @MaxLength(20)
  phone: string;

  @ApiPropertyOptional({ example: 'location', description: '' })
  @IsString()
  @IsOptional()
  @MaxLength(255)
  description: string;
}
