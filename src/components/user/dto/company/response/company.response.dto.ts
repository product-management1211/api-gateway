import { ApiProperty } from '@nestjs/swagger';
import { Expose } from 'class-transformer';
import { SuccessResponse } from 'src/utils/success.response.dto';

export class CompanyDto {
  @ApiProperty({ example: 1, description: '' })
  @Expose()
  id: number;

  @ApiProperty({ example: 'company 1', description: '' })
  @Expose()
  name: string;

  @ApiProperty({ example: 'ABCDEF', description: '' })
  @Expose()
  code: string;

  @ApiProperty({ example: 'Ha Noi', description: '' })
  @Expose()
  address: string;

  @ApiProperty({ example: '0955-015-1458', description: '' })
  @Expose()
  phone: string;

  @ApiProperty({ example: 'tax_no', description: '' })
  @Expose()
  taxNo: string;

  @ApiProperty({ example: 'email@gmail.com', description: '' })
  @Expose()
  email: string;

  @ApiProperty({ example: 'fax', description: '' })
  @Expose()
  fax: string;

  @ApiProperty({ example: 'description', description: '' })
  @Expose()
  description: string;

  @ApiProperty({ example: '2021-07-13 09:13:15.562609+00', description: '' })
  @Expose()
  createdAt: Date;

  @ApiProperty({ example: '2021-07-13 09:13:15.562609+00', description: '' })
  @Expose()
  updatedAt: Date;
}

export class CompanyResponseDto extends SuccessResponse {
  @ApiProperty()
  @Expose()
  data: CompanyDto;
}
