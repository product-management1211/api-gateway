import {
  Body,
  Controller,
  Delete,
  Get,
  Inject,
  Param,
  ParseIntPipe,
  Post,
  Put,
  Query,
  Req,
} from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Public } from 'src/core/decorators/set-public.decorator';
import { SuccessResponse } from 'src/utils/success.response.dto';
import { CreateCompanyRequestDto } from './dto/company/request/create-company.request.dto';
import { GetCompaniesRequestDto } from './dto/company/request/get-companies.request.dto';
import { CompanyResponseDto } from './dto/company/response/company.response.dto';
import { GetCompaniesResponseDto } from './dto/company/response/get-companies.response.dto';
import { CreateFactoryRequestDto } from './dto/factory/create-factory.request.dto';
import { FactoryResponseDto } from './dto/factory/factory.response.dto';
import { GetFactoriesRequestDto } from './dto/factory/get-factories.request.dto';
import { GetFactoriesResponseDto } from './dto/factory/get-factories.response';
import { CreateUserRequestDto } from './dto/user/request/create-user.request.dto';
import { GetListUserRequestDto } from './dto/user/request/get-list-user.request.dto';
import { UpdateUserRequestDto } from './dto/user/request/update-user.request.dto';
import { GetListUserResponseDto } from './dto/user/response/get-list-user.response.dto';
import { UserResponseDto } from './dto/user/response/user.response.dto';

@Controller('users')
@ApiTags('Users')
export class UserController {
  constructor(
    @Inject('USER_SERVICE') private readonly userServiceClient: ClientProxy,
  ) {}

  @Get('ping')
  public async get(): Promise<any> {
    console.log('hello');

    return await this.userServiceClient.send('ping', {});
  }

  //user
  @Public()
  @Post('create')
  @ApiOperation({
    tags: ['User'],
    summary: 'Register User',
    description: 'Register User',
  })
  @ApiResponse({
    status: 200,
    description: 'Register Successfully',
    type: UserResponseDto,
  })
  public async register(@Body() request: CreateUserRequestDto): Promise<any> {
    return await this.userServiceClient.send('create', request);
  }

  @Delete('/:id')
  @ApiOperation({
    tags: ['User'],
    summary: 'Delete User',
    description: 'Delete User',
  })
  @ApiResponse({
    status: 200,
    description: 'Delete successfully',
    type: SuccessResponse,
  })
  public async delete(
    @Param('id', new ParseIntPipe()) id: number,
  ): Promise<any> {
    return await this.userServiceClient.send('delete', id);
  }

  @Get('/list')
  @ApiOperation({
    tags: ['User'],
    summary: 'List User',
    description: 'List User',
  })
  @ApiResponse({
    status: 200,
    description: 'List successfully',
    type: GetListUserResponseDto,
  })
  public async getList(@Query() request: GetListUserRequestDto): Promise<any> {
    return await this.userServiceClient.send('list', request);
  }

  @Get('/list-sync')
  @ApiOperation({
    tags: ['User'],
    summary: 'List User',
    description: 'List User',
  })
  @ApiResponse({
    status: 200,
    description: 'List successfully',
    type: GetListUserResponseDto,
  })
  public async getListSync(
    @Query() request: GetListUserRequestDto,
    @Req() req: any,
  ): Promise<any> {
    console.log('req.user', req.user);

    if (req.user.username === 'admin')
      return await this.userServiceClient.send('list_sync', request);
  }

  @Get('/:id')
  @ApiOperation({
    tags: ['User'],
    summary: 'Detail User',
    description: 'Detail User',
  })
  @ApiResponse({
    status: 200,
    description: 'Get Detail successfully',
    type: UserResponseDto,
  })
  public async detail(
    @Param('id', new ParseIntPipe()) id: number,
  ): Promise<any> {
    return await this.userServiceClient.send('detail', id);
  }

  @Put('/:id')
  @ApiOperation({
    tags: ['User'],
    summary: 'Update User',
    description: 'Update User',
  })
  @ApiResponse({
    status: 200,
    description: 'Update successfully',
    type: UserResponseDto,
  })
  public async update(
    @Param('id', new ParseIntPipe()) id: number,
    @Body() request: UpdateUserRequestDto,
  ): Promise<any> {
    const payload = {
      id: id,
      ...request,
    };
    return await this.userServiceClient.send('update', payload);
  }

  //Company
  @Post('/companies/create')
  @ApiOperation({
    tags: ['companies'],
    summary: 'Create Company',
    description: 'Create new company',
  })
  @ApiResponse({
    status: 200,
    description: 'Create Company Successfully',
    type: CompanyResponseDto,
  })
  public async createCompany(
    @Body() request: CreateCompanyRequestDto,
  ): Promise<any> {
    return await this.userServiceClient.send('company_create', request);
  }

  @Put('/companies/:id')
  @ApiOperation({
    tags: ['Company'],
    summary: 'Update Company Type',
    description: 'Sửa thông tin công ty',
  })
  @ApiResponse({
    status: 200,
    description: 'Update successfully',
    type: CompanyResponseDto,
  })
  public async updateCompany(
    @Param('id', new ParseIntPipe()) id: number,
    @Body() request: CreateCompanyRequestDto,
  ): Promise<any> {
    const payload = {
      id: id,
      ...request,
    };
    return await this.userServiceClient.send('company_update', payload);
  }

  @Delete('/companies/:id')
  @ApiOperation({
    tags: ['Company'],
    summary: 'Delete Company Type',
    description: 'Xóa công ty',
  })
  @ApiResponse({
    status: 200,
    description: 'Delete successfully',
    type: SuccessResponse,
  })
  public async deleteCompany(
    @Param('id', new ParseIntPipe()) id: number,
  ): Promise<any> {
    return await this.userServiceClient.send('company_delete', id);
  }

  @Get('/companies/:id')
  @ApiOperation({
    tags: ['Company'],
    summary: 'Detail Company Type',
    description: 'Chi tiết công ty',
  })
  @ApiResponse({
    status: 200,
    description: 'Get Detail successfully',
    type: CompanyResponseDto,
  })
  public async getDetailCompany(
    @Param('id', new ParseIntPipe()) id: number,
  ): Promise<any> {
    return await this.userServiceClient.send('company_detail', id);
  }

  @Get('/companies/list')
  @ApiOperation({
    tags: ['Company'],
    summary: 'List Company Type',
    description: 'Danh sách công ty',
  })
  @ApiResponse({
    status: 200,
    description: 'Get List successfully',
    type: GetCompaniesResponseDto,
  })
  public async getListCompany(
    @Query() request: GetCompaniesRequestDto,
  ): Promise<any> {
    return await this.userServiceClient.send('company_list', request);
  }

  @Put('/companies/:id/confirm')
  @ApiOperation({
    tags: ['Company'],
    summary: 'Confirm Company',
    description: 'Xác nhận công ty',
  })
  @ApiResponse({
    status: 200,
    description: 'Confirm successfully',
    type: CompanyResponseDto,
  })
  public async confirmCompany(
    @Param('id', new ParseIntPipe()) id,
    @Req() req: any,
  ): Promise<any> {
    const payload = {
      id: id,
      userId: req.user.id,
    };
    return await this.userServiceClient.send('confirm_company', payload);
  }

  @Put('/companies/:id/reject')
  @ApiOperation({
    tags: ['Company'],
    summary: 'Reject Company',
    description: 'từ chối xác nhận công ty',
  })
  @ApiResponse({
    status: 200,
    description: 'Reject successfully',
    type: CompanyResponseDto,
  })
  public async rejectCompany(
    @Param('id', new ParseIntPipe()) id,
    @Req() req: any,
  ): Promise<any> {
    const payload = {
      id: id,
      userId: req.user.id,
    };
    return await this.userServiceClient.send('reject_company', payload);
  }

  //factory
  @Post('/factories/create')
  @ApiOperation({
    tags: ['Factory'],
    summary: 'Create Factory',
    description: 'Create new Factory',
  })
  @ApiResponse({
    status: 200,
    description: 'Create Factory Successfully',
    type: FactoryResponseDto,
  })
  public async createFactory(
    @Body() request: CreateFactoryRequestDto,
  ): Promise<any> {
    return await this.userServiceClient.send('create_factory', request);
  }

  @Put('/factories/:id')
  @ApiOperation({
    tags: ['Factory'],
    summary: 'Update Factory Type',
    description: 'Sửa thông tin nhà máy',
  })
  @ApiResponse({
    status: 200,
    description: 'Update successfully',
    type: FactoryResponseDto,
  })
  public async updateFactory(
    @Param('id', new ParseIntPipe()) id: number,
    @Body() request: CreateFactoryRequestDto,
  ): Promise<any> {
    const payload = {
      id: id,
      ...request,
    };
    return await this.userServiceClient.send('update_factory', payload);
  }

  @Delete('/factories/:id')
  @ApiOperation({
    tags: ['Factory'],
    summary: 'Delete Factory Type',
    description: 'Xóa nhà máy',
  })
  @ApiResponse({
    status: 200,
    description: 'Delete successfully',
    type: SuccessResponse,
  })
  public async deleteFactory(
    @Param('id', new ParseIntPipe()) id: number,
  ): Promise<any> {
    return await this.userServiceClient.send('delete_factory', id);
  }

  @Get('/factories/:id')
  @ApiOperation({
    tags: ['Factory'],
    summary: 'Detail Factory Type',
    description: 'Chi tiết nhà máy',
  })
  @ApiResponse({
    status: 200,
    description: 'Get Factory Detail successfully',
    type: FactoryResponseDto,
  })
  public async getDetailFactory(
    @Param('id', new ParseIntPipe()) id: number,
  ): Promise<any> {
    return await this.userServiceClient.send('detail_factory', id);
  }

  @Get('/factories/list')
  @ApiOperation({
    tags: ['Factory'],
    summary: 'List Factory Type',
    description: 'Danh sách nhà máy',
  })
  @ApiResponse({
    status: 200,
    description: 'Get Factory List successfully',
    type: GetFactoriesResponseDto,
  })
  public async getListFactory(
    @Query() request: GetFactoriesRequestDto,
  ): Promise<any> {
    return await this.userServiceClient.send('list_factories', request);
  }

  @Put('/factories/:id/confirm')
  @ApiOperation({
    tags: ['Factory'],
    summary: 'Confirm Factory',
    description: 'Xác nhận nhà máy',
  })
  @ApiResponse({
    status: 200,
    description: 'Confirm successfully',
    type: FactoryResponseDto,
  })
  public async confirmFactory(
    @Param('id', new ParseIntPipe()) id,
    @Req() req: any,
  ): Promise<any> {
    const payload = {
      id: id,
      userId: req.user.id,
    };
    return await this.userServiceClient.send('confirm_factory', payload);
  }

  @Put('/factories/:id/reject')
  @ApiOperation({
    tags: ['Factory'],
    summary: 'Reject Factory',
    description: 'Từ chối xác nhận nhà máy',
  })
  @ApiResponse({
    status: 200,
    description: 'Reject successfully',
    type: FactoryResponseDto,
  })
  public async rejectFactory(
    @Param('id', new ParseIntPipe()) id,
    @Req() req: any,
  ): Promise<any> {
    const payload = {
      id: id,
      userId: req.user.id,
    };
    return await this.userServiceClient.send('reject_factory', payload);
  }
}
