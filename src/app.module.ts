import { Module } from '@nestjs/common';
import { AppService } from './app.service';
import { ConfigModule } from '@nestjs/config';
import { ClientProxyFactory } from '@nestjs/microservices';
import { ConfigService } from './config/config.service';
import { AppController } from './components/app/app.controller';
import { UserController } from './components/user/user.controller';
import { SocketModule } from './socket/socket.module';
import { WarehouseController } from './components/warehouse/warehouse.controller';
import { AuthController } from './components/auth/auth.controller';
import { APP_GUARD } from '@nestjs/core';
import { JwtGuard } from './core/guards/jwt.guard';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    SocketModule,
  ],
  controllers: [
    AppController,
    UserController,
    WarehouseController,
    AuthController,
  ],
  providers: [
    ConfigService,
    {
      provide: APP_GUARD,
      useClass: JwtGuard,
    },
    {
      provide: 'USER_SERVICE',
      useFactory: (config: ConfigService) => {
        const userServiceOptions = config.get('userService');
        return ClientProxyFactory.create(userServiceOptions);
      },
      inject: [ConfigService],
    },
    {
      provide: 'WAREHOUSE_SERVICE',
      useFactory: (config: ConfigService) => {
        const warehouseServiceOptions = config.get('warehouseService');
        return ClientProxyFactory.create(warehouseServiceOptions);
      },
      inject: [ConfigService],
    },
    AppService,
  ],
})
export class AppModule {}
