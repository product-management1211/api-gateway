import {
  CallHandler,
  ExecutionContext,
  HttpException,
  Injectable,
  NestInterceptor,
} from '@nestjs/common';
import { catchError, Observable } from 'rxjs';
import { ResponseCodeEnum } from 'src/constants/response-code.enum';

@Injectable()
export class ExceptionInterceptor implements NestInterceptor {
  intercept(
    context: ExecutionContext,
    next: CallHandler<any>,
  ): Observable<any> | Promise<Observable<any>> {
    return next.handle().pipe(
      catchError((err) => {
        switch (err.message) {
          case 'Forbidden resource':
            throw new HttpException(err.message, ResponseCodeEnum.FORBIDDEN);
          default:
            throw err;
        }
      }),
    );
  }
}
