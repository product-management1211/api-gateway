import {
  CanActivate,
  ExecutionContext,
  HttpException,
  Inject,
  Injectable,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { ClientProxy } from '@nestjs/microservices';
import { lastValueFrom } from 'rxjs';
import { IS_PUBLIC_KEY } from '../decorators/set-public.decorator';

@Injectable()
export class JwtGuard implements CanActivate {
  constructor(
    private reflector: Reflector,
    @Inject('USER_SERVICE')
    private readonly client: ClientProxy,
  ) {}

  async canActivate(context: ExecutionContext): Promise<any> {
    const isPublic = this.reflector.getAllAndOverride<boolean>(IS_PUBLIC_KEY, [
      context.getHandler(),
      context.getClass(),
    ]);
    if (isPublic) {
      return true;
    }

    const req = await context.switchToHttp().getRequest();
    console.log('req', req.headers);

    const res = await lastValueFrom(
      this.client.send('validate_token', {
        jwt: req.headers['authorization']?.split(' ')[1],
      }),
    );

    if (res) {
      if (res.statusCode !== 200) {
        throw new HttpException(res.message, res.status);
      }
      req.user = res.data;
      return true;
    }
    return false;
  }
}
