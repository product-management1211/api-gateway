FROM node:14-alpine AS develop

# Create app directory
WORKDIR /app

COPY package*.json ./

RUN npm cache clean --force

COPY package*.json ./

RUN npm install 

COPY . .

RUN npm run build

# PRODUCTION